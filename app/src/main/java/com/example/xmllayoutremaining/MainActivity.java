package com.example.xmllayoutremaining;

import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    public RelativeLayout mDrawerList;
    public DrawerLayout mDrawerLayout;

    TextView Account,Training_Videos,Notifications,About,Contact,ShareApp,Logout,Username;

    ImageView drawer,Avtar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details);

        /*mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RelativeLayout) findViewById(R.id.left_drawer);

        drawer = (ImageView)findViewById(R.id.imageView);
        drawer.setOnClickListener(this);

        Avtar = (ImageView) findViewById(R.id.avatar);
        Account.setOnClickListener(this);

        Username = (TextView) findViewById(R.id.Username);
        Username.setOnClickListener(this);

        Account = (TextView) findViewById(R.id.txt_Account);
        Account.setOnClickListener(this);

        Training_Videos = (TextView) findViewById(R.id.txt_Training_Videos);
        Training_Videos.setOnClickListener(this);

        Notifications = (TextView) findViewById(R.id.txt_Notificatios);
        Notifications.setOnClickListener(this);

        About = (TextView) findViewById(R.id.txt_About);
        About.setOnClickListener(this);

        Contact = (TextView) findViewById(R.id.txt_Contact);
        Contact.setOnClickListener(this);

        ShareApp = (TextView) findViewById(R.id.txt_shareapp);
        ShareApp.setOnClickListener(this);

        Logout = (TextView) findViewById(R.id.txt_Logout);
        Logout.setOnClickListener(this);*/


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.imageView:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {

                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;

            case R.id.txt_Account:
                Toast.makeText(this,"Account",Toast.LENGTH_LONG).show();

                break;

            case R.id.txt_Training_Videos:
                Toast.makeText(this,"Trainging Videos",Toast.LENGTH_LONG).show();

                break;

            case R.id.txt_Notificatios:
                Toast.makeText(this,"Notifications",Toast.LENGTH_LONG).show();

                break;

            case R.id.txt_About:
                Toast.makeText(this,"About",Toast.LENGTH_LONG).show();

                break;

            case R.id.txt_Contact:
                Toast.makeText(this,"Contact",Toast.LENGTH_LONG).show();

                break;
            case R.id.txt_shareapp:
                Toast.makeText(this,"Shareapp",Toast.LENGTH_LONG).show();

                break;
            case R.id.txt_Logout:
                Toast.makeText(this,"logout",Toast.LENGTH_LONG).show();

                break;
        }
    }
}
